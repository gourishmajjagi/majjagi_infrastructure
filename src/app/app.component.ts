import { Component, AfterViewInit } from '@angular/core';
import { DeviceInfoService } from './services/device-info.service';
import { register } from 'swiper/element/bundle';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit  {
  title = 'majjagi-infrastructure';

  constructor(readonly deviceInfoService: DeviceInfoService) {
  }

  ngAfterViewInit(): void {
    register();
  }
}

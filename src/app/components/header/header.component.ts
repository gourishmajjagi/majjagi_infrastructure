import { Component } from '@angular/core';
import { DeviceInfoService } from 'src/app/services/device-info.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  constructor(readonly deviceInfoService: DeviceInfoService) {
  }
}

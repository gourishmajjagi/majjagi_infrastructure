import { ViewportScroller } from '@angular/common';
import { Component } from '@angular/core';
import { DeviceInfoService } from 'src/app/services/device-info.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent {

  menuItems: { name: string; link: string }[] = [
    {
      name: "Home",
      link: "home"
    },
    {
      name: "About us",
      link: "about"
    },
    {
      name: "Services",
      link: "service"
    },
    {
      name: "Contact us",
      link: "contact"
    }
  ];

  appendBG = false;

  constructor(readonly deviceInfoService: DeviceInfoService,
    private scroller: ViewportScroller) {
    this.scrollListner();
  }

  scrollListner = () => {

    window.addEventListener('scroll', (event)=> {
        if (window.pageYOffset > 300) {
          this.appendBG = true;
        } else {
          this.appendBG = false;
        }

    })
  }

  navTo = (route: string) => {
    console.log(route);

    this.scroller.scrollToAnchor(route);
  }
}

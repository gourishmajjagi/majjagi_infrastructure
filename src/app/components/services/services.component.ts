import { Component } from '@angular/core';
import { DeviceInfoService } from 'src/app/services/device-info.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent {
  services = [
    {
      title: 'Residential Land Development',
      description: 'From our humble beginnings, we’ve continued to march towards a prosperous future that will see us becoming, for all intents and purposes, an immortal brand.',
      imageURL: 'assets/images/service/development.jpg'
    },
    {
      title: 'Turnkey Construction',
      description: 'From our humble beginnings, we’ve continued to march towards a prosperous future that will see us becoming, for all intents and purposes, an immortal brand.',
      imageURL: 'assets/images/service/blueprint.jpg'
    },
    {
      title: 'Interior Design',
      description: 'From our humble beginnings, we’ve continued to march towards a prosperous future that will see us becoming, for all intents and purposes, an immortal brand.',
      imageURL: 'assets/images/service/interior.jpg'
    },
    {
      title: 'Smart House Solution',
      description: 'From our humble beginnings, we’ve continued to march towards a prosperous future that will see us becoming, for all intents and purposes, an immortal brand.',
      imageURL: 'assets/images/service/smart-home.jpg'
    }
  ]

  constructor(readonly deviceInfoService: DeviceInfoService) {}
}

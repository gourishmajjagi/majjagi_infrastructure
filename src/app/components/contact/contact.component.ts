import { Component } from '@angular/core';
import { DeviceInfoService } from 'src/app/services/device-info.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent {

  constructor(readonly deviceInfoService: DeviceInfoService) {}
}
